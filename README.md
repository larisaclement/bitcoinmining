# BitcoinMining
Coursework for Cryptography & Security to observe the process of mining a given bitcoin block.

# Usage
The program treats 2 cases. 
````
case1(): The first case which requires finding a "lucky" nonce for the given block. Will be referred to as nonce1.
case1_first_five(): Part of the first case; displays the first 5 computed nonces and hash values in case1().
case2(): The second case requires random intiliasation of the starting nonce based on nonce1. Then, attempt finding a "lucky" nonce.
````

# Results
Case 1:
```
result = successful
nonce1 = 3060331852
block hash = 0000000000000000000d7612d743325d8e47cb9e506d547694478f35f736188e
```
Case 2:
```
result = failed to find 'lucky' nonce
starting nonce = 3152612925
iterations = 10^9
```

# Disclaimer
The code was adapted from [shirriff/mine.py](https://gist.github.com/shirriff/cd5c66da6ba21a96bb26).
