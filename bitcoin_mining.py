#--------------------------------------------------------------------------
# Coursework - Bitcoin mining
# Clement Larisa, grupa 342
# 14/04/2020
#--------------------------------------------------------------------------
import hashlib, struct
import codecs
import random
#----------------------------------------------------------------------
# ---------------------------------------------------------------------
#   Initialisations
# ---------------------------------------------------------------------
#----------------------------------------------------------------------
ver = 0x20400000
prev_block = "00000000000000000006a4a234288a44e715275f1775b77b2fddb6c02eb6b72f"
#  A special hash of all the transactions in the block. 
mrkl_root = "2dc60c563da5368e0668b81bc4d8dd369639a1134f68e425a9a74e428801e5b8"
#  Tuesday, October 29, 2019 11:13:02 PM GMT+02:00
time_ = 0x5DB8AB5E
#  The mining difficulty value 
bits = 0x17148EDF
exp = bits >> 24
mant = bits & 0xffffff
target_hexstr = '%064x' % (mant * (1<<(8*(exp - 3))))
target_str = codecs.decode(target_hexstr,'hex')

#--------------------------------------------------------------------------
#  Computes and displays the first five hash values starting with nonce =
#  3,000,000,000.
#--------------------------------------------------------------------------
def case1_first_five():
    nonce = 3000000000
    calc_hash(nonce, nonce + 5, 1)
#end

#--------------------------------------------------------------------------
#  Performs 100,000,000 iterations starting with nonce = 3,000,000,000 in
#  order to find a suitable hash value to add to the blockchain.
#--------------------------------------------------------------------------
def case1():
    #----------------------------------------------------------------------
    # ---------------------------------------------------------------------
    #   Result: SUCCESSFUL
    #   nonce1 = 3060331852
    #   hash = 0000000000000000000d7612d743325d8e47cb9e506d547694478f35f736188e
    # ---------------------------------------------------------------------
    #----------------------------------------------------------------------
    nonce = 3000000000
    calc_hash(nonce, nonce + 100000000, 0)
#end
    
#--------------------------------------------------------------------------
#  Performs 1,000,000,000 iterations starting with a random nonce in
#  (nonce1, nonce1 + 100,000,000).
#--------------------------------------------------------------------------
def case2():
    nonce1 = 3060331852
    nonce2 = random.randrange(nonce1 + 1, nonce1 + 100000000)
    print("case2(): nonce2: ")
    print(nonce2)
    #  Result: FAILED
    #  Did not manage to find nonce
    calc_hash(nonce2, nonce2 + 1000000000, 0)
#end

#--------------------------------------------------------------------------
#  Computes a hash value that can be added to the blockchain, starting from
#  given nonce and in a given number of iterations.
#  
#  nonce: int - starting nonce.
#  stop: int - nonces upper bound.
#  display: 0/1 - whether to display all computed hash values.
#--------------------------------------------------------------------------
def calc_hash(nonce, stop, display):
    did_find_hash = 0
    print("Commencing calculations... This will take a while...")
    while nonce < stop:
        header = ( struct.pack("<L", ver) + codecs.decode(prev_block, 'hex')[::-1] +
          codecs.decode(mrkl_root, 'hex')[::-1] + struct.pack("<LLL", time_, bits, nonce))
        hash = hashlib.sha256(hashlib.sha256(header).digest()).digest()
        if(display == 1):
            print(nonce)
            print(codecs.encode(hash[::-1],'hex'))
        #end
        if hash[::-1] < target_str:
            did_find_hash = 1
            print("calc_hash: SUCCESFULLY FOUND:")
            print("calc_hash: nonce: ")
            print(nonce)
            print("calc_hash: hash: ")
            print(codecs.encode(hash[::-1],'hex'))
            break
        #end
        nonce += 1
    #end
    if did_find_hash == 0:
        print("calc_hash: FAILED. No suitable hash found in range.")
    #end
#end

